package util;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class WebDriverManager {

	
	public static WebDriver getFirefoxWebDriver(){
		return new FirefoxDriver();
	}
	
	public static WebDriver getChromeWebDriver() throws MalformedURLException{
		URL url = new URL("http://localhost:9515");
		return new RemoteWebDriver(url, DesiredCapabilities.chrome());
	}
	
}
