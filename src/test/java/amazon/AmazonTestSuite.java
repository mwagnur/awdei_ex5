package amazon;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;



@RunWith(Suite.class)
@Suite.SuiteClasses({
        Amazon1.class,
        Amazon2.class,
        Amazon3.class
})
public class AmazonTestSuite {
	
}
