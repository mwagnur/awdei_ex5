package amazon;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Amazon2{
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://www.amazon.de/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAmazon2Search() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.id("twotabsearchtextbox")).clear();
    driver.findElement(By.id("twotabsearchtextbox")).sendKeys("kindle");
    driver.findElement(By.cssSelector("input.nav-submit-input")).click();
    String searchVar = driver.findElement(By.cssSelector("#result_2 > h3.newaps > a > span.lrg.bold")).getText();
    System.out.println("searchVar = " + searchVar);
    new Select(driver.findElement(By.id("searchDropdownBox"))).selectByVisibleText("Baby");
    driver.findElement(By.cssSelector("option[value=\"search-alias=baby\"]")).click();
    driver.findElement(By.id("twotabsearchtextbox")).clear();
    driver.findElement(By.id("twotabsearchtextbox")).sendKeys(searchVar);
    driver.findElement(By.cssSelector("input.nav-submit-input")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (isElementPresent(By.cssSelector("span.noResultsTitleKeyword"))) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    try {
      assertEquals("\"Kindle Fire HD 7, 17 cm (7 Zoll), HD-Display, WLAN, 8 GB - Mit Spezialangeboten\"", driver.findElement(By.cssSelector("span.noResultsTitleKeyword")).getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    new Select(driver.findElement(By.id("searchDropdownBox"))).selectByVisibleText("Elektronik & Foto");
    driver.findElement(By.cssSelector("option[value=\"search-alias=electronics\"]")).click();
    driver.findElement(By.id("twotabsearchtextbox")).clear();
    driver.findElement(By.id("twotabsearchtextbox")).sendKeys(searchVar);
    driver.findElement(By.cssSelector("input.nav-submit-input")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (isElementPresent(By.id("s-result-count"))) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    try {
      assertTrue(isElementPresent(By.id("s-result-count")));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    
    // ERROR: Caught exception [ERROR: Unsupported command [getEval | storedVars['searchVar'].substr(0,storedVars['searchVar'].indexOf(',')) | ]]  
    String newSearchVar= searchVar.substring(0, searchVar.indexOf(','));
    System.out.println("newSearchVar = " + newSearchVar);
   
    driver.findElement(By.id("twotabsearchtextbox")).clear();
    driver.findElement(By.id("twotabsearchtextbox")).sendKeys(newSearchVar);
    driver.findElement(By.cssSelector("input.nav-submit-input")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (driver.findElement(By.id("rightContainerATF")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (driver.findElement(By.id("result_0")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (driver.findElement(By.cssSelector("span.lrg.bold")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    String price = driver.findElement(By.xpath("//div[@id='result_0']/ul/li/div/a/span")).getText();
    driver.findElement(By.cssSelector("span.lrg.bold")).click();
    try {
      assertEquals(price, driver.findElement(By.cssSelector("b.priceLarge.kitsunePrice")).getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    System.out.println(verificationErrorString);
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
