package amazon;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import util.WebDriverManager;

public class Amazon1 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		driver = WebDriverManager.getFirefoxWebDriver();

		// driver = WebDriverManager.getChromeWebDriver();
		baseUrl = "https://www.amazon.de/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testAmazon1() throws Exception {
		driver.get(baseUrl + "/");
		driver.findElement(By.id("twotabsearchtextbox")).clear();
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys("kindle");
		driver.findElement(By.cssSelector("input.nav-submit-input")).click();
		driver.findElement(By.cssSelector("span.lrg.bold")).click();
		driver.findElement(By.id("bb_chk_B005DOKG70")).click();
		driver.findElement(By.id("bb_chk_B0082HGBAG")).click();
		driver.findElement(By.id("bb_atc_button")).click();
		try {
			assertFalse(driver.findElement(By.id("bb_chk_B00F6E8IH2")).isSelected());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		try {
			assertTrue(driver.findElement(By.id("bb_chk_B0082HGBAG")).isSelected());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		try {
			assertTrue(driver.findElement(By.id("bb_chk_B005DOKG70")).isSelected());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		driver.findElement(By.id("bb_noThanks_link")).click();

		assertEquals("3 Artikel wurden zum Einkaufswagen hinzugefügt", driver.findElement(By.id("confirm-text"))
				.getText());

		driver.findElement(By.cssSelector("#nav-cart > span.nav-button-title.nav-button-line1")).click();
		assertEquals("Amazon.de Einkaufswagen", driver.getTitle());

		System.out.println("in Einkaufswagen");
		try {
			assertEquals(
					"Kindle, 15 cm (6 Zoll) E Ink-Display, WLAN, Schwarz",
					driver.findElement(
							By.xpath("//form[@id='activeCartViewForm']/div[2]/div/div[4]/div[2]/div/div/div/div[2]/ul/li/span/a/span"))
							.getText());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		
		try {
			assertEquals(
					"Netzteil für eReader, Kindle EU USB-Ladegerät, EU (geeignet für Kindle Paperwhite, Kindle, Kindle Touch und Kindle Keyboard)",
					driver.findElement(
							By.xpath("//form[@id='activeCartViewForm']/div[2]/div[2]/div[4]/div[2]/div/div/div/div[2]/ul/li/span/a/span"))
							.getText());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		try {
			assertEquals(
					"Amazon Kindle Lederhülle, Schwarz (nur geeignet für Kindle)",
					driver.findElement(
							By.xpath("//form[@id='activeCartViewForm']/div[2]/div[3]/div[4]/div[2]/div/div/div/div[2]/ul/li/span/a/span"))
							.getText());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}

		
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By
				.cssSelector("input[name=\"submit.delete.CZUL9X8CYNGSN\"]")));
		
		driver.findElement(By.cssSelector("input[name=\"submit.delete.CZUL9X8CYNGSN\"]")).click();

		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By
				.cssSelector("div.sc-list-item-removed-msg.a-padding-medium")));	

		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By
				.name("submit.delete.C32OVKGLUUBMED")));

		driver.findElement(By.name("submit.delete.C32OVKGLUUBMED")).click();

		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By
				.name("submit.delete.CCYQXJ02NHYD3")));

		driver.findElement(By.name("submit.delete.CCYQXJ02NHYD3")).click();

		new WebDriverWait(driver, 100);

		System.out.println("Amazon1 test done");
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();

		System.out.println(verificationErrorString);
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

}
