package amazon;

import java.net.URL;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import util.WebDriverManager;

public class Amazon3 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		driver = WebDriverManager.getChromeWebDriver();

		baseUrl = "http://www.amazon.de/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testAmazon3() throws Exception {
		driver.get(baseUrl + "/");
		driver.findElement(By.linkText("Gutscheine")).click();

		driver.findElement(By.linkText("Mit Animation")).click();
		driver.findElement(
				By.cssSelector("area[alt=\"Gutscheine mit Animation\"]"))
				.click();
		driver.findElement(By.cssSelector("span.lrg.bold")).click();
		try {
			assertTrue(isElementPresent(By
					.cssSelector("img.egcDesignPreviewOverlay")));
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		driver.findElement(By.id("egcForm-amount-EmailDesigns")).clear();
		driver.findElement(By.id("egcForm-amount-EmailDesigns")).sendKeys(
				"0,15");
		driver.findElement(By.id("egcForm-recipientEmailSingle-EmailDesigns"))
				.clear();
		driver.findElement(By.id("egcForm-recipientEmailSingle-EmailDesigns"))
				.sendKeys("testAW@hotmail.com");
		driver.findElement(By.id("egcForm-senderName-EmailDesigns")).clear();
		driver.findElement(By.id("egcForm-senderName-EmailDesigns")).sendKeys(
				"AWDEI Group 2");
		driver.findElement(By.id("egcForm-message-EmailDesigns")).clear();
		driver.findElement(By.id("egcForm-message-EmailDesigns")).sendKeys(
				"Hej, this is a test message!");
		driver.findElement(By.id("egcForm-quantity-EmailDesigns")).clear();
		driver.findElement(By.id("egcForm-quantity-EmailDesigns")).sendKeys(
				"10");
		driver.findElement(By.id("egcForm-deliveryDate-EmailDesigns")).click();

		// (new WebDriverWait(driver,
		// 10)).until(ExpectedConditions.textToBePresentInElementLocated(By.id("egcForm-numCards-EmailDesigns"),
		// "10 Gutscheine:"));

		driver.findElement(
				By.cssSelector("#egcForm-addToEmptyCart-EmailDesigns > span > input[type=\"submit\"]"))
				.click();

		for (int second = 0;; second++) {
			if (second >= 60)
				fail("timeout");
			try {
				if (isElementPresent(By
						.cssSelector("div.egcCart-lineItemWrapper.egcCf")))
					break;
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		}

		try {
			assertTrue(isElementPresent(By
					.xpath("//div[@id='egcCart-lineItemSlots']/div/div/div[2]/p[2]")));
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		try {
			assertEquals("10 Gutschein(e):",
					driver.findElement(By.id("egcCart-numCards")).getText());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		driver.findElement(
				By.xpath("//div[@id='egcCart-lineItemSlots']/div/div/div[2]/p[4]/a/span"))
				.click();

		System.out.println("clicking Bearbeiten");
		try {
			assertEquals(
					"testAW@hotmail.com",
					driver.findElement(
							By.id("egcForm-recipientEmailSingle-EmailDesigns"))
							.getAttribute("value"));
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		try {
			assertEquals(
					"AWDEI Group 2",
					driver.findElement(By.id("egcForm-senderName-EmailDesigns"))
							.getAttribute("value"));
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		try {
			assertEquals("Hej, this is a test message!",
					driver.findElement(By.id("egcForm-message-EmailDesigns"))
							.getAttribute("value"));
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		new Select(driver.findElement(By
				.id("egcForm-amountSelect-EmailDesigns")))
				.selectByVisibleText("EUR 20,00");
		driver.findElement(By.id("egcForm-quantity-EmailDesigns")).clear();
		driver.findElement(By.id("egcForm-quantity-EmailDesigns"))
				.sendKeys("1");
		driver.findElement(By.id("egcForm-deliveryDate-EmailDesigns")).click();

		(new WebDriverWait(driver, 10))
		.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("p.egcForm-addToCartNotification")));

		(new WebDriverWait(driver, 10))
		.until(ExpectedConditions.elementToBeClickable(By
				.cssSelector("#egcForm-update-EmailDesigns > span > input[type=\"submit\"]")));

		
		driver.findElement(
				By.cssSelector("#egcForm-update-EmailDesigns > span > input[type=\"submit\"]"))
				.click();
		
		(new WebDriverWait(driver, 10))
		.until(ExpectedConditions.invisibilityOfElementLocated(By.id("egcCart-loading")));


		(new WebDriverWait(driver, 10)).until(ExpectedConditions
				.textToBePresentInElementLocated(By.id("egcCart-numCards"),
						"1 Gutschein(e):"));

		try {
			assertEquals("1 Gutschein(e):",
					driver.findElement(By.id("egcCart-numCards")).getText());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		try {
			assertEquals("EUR 20,00",
					driver.findElement(By.id("egcCart-orderTotal")).getText());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
		driver.findElement(By.id("egcCart-clearCartLink")).click();
		driver.findElement(
				By.cssSelector("#egcCart-clearCartAccept > span > input[type=\"submit\"]"))
				.click();
		try {
			assertTrue(isElementPresent(By.id("egcCart-empty")));
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		System.out.println(verificationErrorString);
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
}
